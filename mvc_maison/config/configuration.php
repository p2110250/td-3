<?php

const DEBUG = true; // production : false; dev : true

// Accès base de données
const BD_HOST = 'localhost';
const BD_DBNAME = 'p2110250';
const BD_USER = 'p2110250';
const BD_PWD = '622984';

// Langue du site
const LANG = 'FR-fr';

// Paramètres du site : nom de l'auteur ou des auteurs
const AUTEUR = 'Matéo';

const TITRE_PAGE_ACCUEIL_TOUS = 'Accueil';

const IDENTIFIANT_ADMIN = 'admin';
const MOT_DE_PASSE = 'p4ss4dm1n';

//dossiers racines du site
define('PATH_CONTROLLERS', './controllers/c_');
define('PATH_ENTITY', './entities/');
define('PATH_ASSETS', './assets/');
define('PATH_LIB', './lib/');
define('PATH_MODELS', './models/');
define('PATH_VIEWS', './views/v_');
define('PATH_TEXTES', './languages/');

//sous dossiers
define('PATH_CSS', PATH_ASSETS . 'css/');
define('PATH_IMAGES', PATH_ASSETS . 'images/');
define('PATH_SCRIPTS', PATH_ASSETS . 'scripts/');

//fichiers
define('PATH_LOGO', PATH_IMAGES . 'logo.png');
define('PATH_MENU', PATH_VIEWS . 'menu.php');
