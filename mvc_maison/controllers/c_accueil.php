<?php
require_once(PATH_MODELS . 'DAO.php');
require_once(PATH_MODELS . 'accueil.php');

$allPhotos = new Accueil(true);

if (isset($_GET['categorie'])) {
    $photos = $allPhotos->getPhotos($_GET['categorie']);
} else {
    $photos = $allPhotos->getPhotos('all');
}


require_once(PATH_VIEWS . $page . '.php');
