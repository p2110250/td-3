<?php
require_once(PATH_MODELS . 'DAO.php');
require_once(PATH_MODELS . 'ajout.php');
if (isset($_FILES['photo']['type']) && isset($_FILES['photo']['size']) && isset($_POST['description']) && isset($_POST['categorie'])) {
    if (preg_match('/(jpeg|gif|png|jpg)$/', $_FILES['photo']['type'])) {
        if ($_FILES['photo']['size'] <= (100 * 1024)) {
            if (preg_match('#[a-zA-Z]#', $_POST['description'])) {
                if ($_POST['categorie'] == '1' || $_POST['categorie'] == '2' || $_POST['categorie'] == '3') {
                    $ajout = new Ajout(True);
                    $ajout->ajoutPhoto();
                    $alert['messageAlert'] = 'Photo ajoutée';
                    $alert['classAlert'] = 'success';
                } else {
                    $alert['messageAlert'] = 'La catégorie n\'est pas valide';
                    $alert['classAlert'] = 'danger';
                }
            } else {
                $alert['messageAlert'] = 'La description n\'est pas valide';
                $alert['classAlert'] = 'danger';
            }
        } else {
            $alert['messageAlert'] = 'Le fichier est trop gros';
            $alert['classAlert'] = 'danger';
        }
    } else {
        $alert['messageAlert'] = 'Le fichier n\'est pas une image';
        $alert['classAlert'] = 'danger';
    }
}

require_once(PATH_VIEWS . 'ajout.php');
