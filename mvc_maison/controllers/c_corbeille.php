<?php
require_once(PATH_MODELS . 'DAO.php');
require_once(PATH_MODELS . 'corbeille.php');

if (isset($_GET['incorb'])) {
    $corbeille = new Corbeille(true);
    $corbeille->deplaceToCorbeille();
    $alert['messageAlert'] = 'La photo a été mise à la corbeille';
    $alert['classAlert'] = 'success';
}

if (isset($_GET['restore'])) {
    $restorePhoto = new Corbeille(true);
    $restorePhoto->restorePhoto($_GET['restore']);
    $alert['messageAlert'] = 'La photo a été restaurée';
    $alert['classAlert'] = 'success';
}
if (isset($_GET['delete'])) {
    $deletePhoto = new Corbeille(true);
    $deletePhoto->deletePhoto($_GET['delete']);
    $alert['messageAlert'] = 'La photo a été supprimée';
    $alert['classAlert'] = 'success';
}
if (isset($_GET['deleteAll'])) {
    $deleteAll = new Corbeille(true);
    $deleteAll->deleteAll();
    $alert['messageAlert'] = 'La corbeille a été vidée';
    $alert['classAlert'] = 'success';
}
if (isset($_GET['restoreAll'])) {
    $restoreAll = new Corbeille(true);
    $restoreAll->restoreAll();
    $alert['messageAlert'] = 'Toutes les photos ont été restaurées';
    $alert['classAlert'] = 'success';
}

$photos = new Corbeille(true);
$photos = $photos->getPhotos();

require_once(PATH_VIEWS . 'corbeille.php');
