<?php
class Accueil extends DAO
{
    public function getPhotos($categorie)
    {
        $sql = "SELECT * FROM Photo";
        if ($categorie != 'all') {
            $sql .= " WHERE catId = " . $categorie;
        }
        $result = $this->queryAll($sql);
        return $result;
    }
}
