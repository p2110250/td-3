<?php
class Corbeille extends DAO
{
    public function getPhotos()
    {
        $sql = 'SELECT photoId, nomFich, description, catId, nomCat FROM Corbeille NATURAL JOIN Categorie';
        $result = $this->queryAll($sql);
        return $result;
    }
    public function deplaceToCorbeille()
    {
        $sql = 'INSERT INTO Corbeille (photoId, nomFich, description, catId) SELECT NULL, nomFich, description, catId FROM Photo WHERE photoId = ?';
        $this->queryRow($sql, array($_GET['incorb']));
        $sql = 'DELETE FROM Photo WHERE photoId = ' . $_GET['incorb'];
        $result = $this->queryRow($sql);
    }
    public function restorePhoto($photoId)
    {
        $sql = 'INSERT INTO Photo (photoId, nomFich, description, catId) SELECT NULL, nomFich, description, catId FROM Corbeille WHERE photoId = ?';
        $this->queryRow($sql, array($photoId));
        $sql = 'DELETE FROM Corbeille WHERE photoId = ?';
        $this->queryRow($sql, array($photoId));
    }
    public function deletePhoto($photoId)
    {
        $sql = 'DELETE FROM Corbeille WHERE photoId = ?';
        $this->queryRow($sql, array($photoId));
    }
    public function deleteAll()
    {
        $sql = 'DELETE FROM Corbeille';
        $this->queryALL($sql);
    }
    public function restoreAll()
    {
        $sql = 'INSERT INTO Photo (photoId, nomFich, description, catId) SELECT NULL, nomFich, description, catId FROM Corbeille';
        $this->queryRow($sql);
        $sql = 'DELETE FROM Corbeille';
        $this->queryRow($sql);
    }
}
