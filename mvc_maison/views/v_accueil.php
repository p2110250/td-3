<?php
//  En tête de page
?>
<?php require_once(PATH_VIEWS . 'header.php'); ?>

<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS . 'alert.php'); ?>

<!--  Nombre de photos affichées -->
<div> <?php echo count($photos) ?> photos affichées </div>
<p>Quelles photos souhaitez-vous afficher ?</p>
<!--  Formulaire de sélection des photos -->
<form>
    <select name="categorie">
        <option value="all">Toutes les photos</option>
        <option value="1">Animaux</option>
        <option value="2">Repas</option>
        <option value="3">Monuments</option>
    </select>
    <input type="submit" value="Valider" />
</form>

<!--  Début de la page -->
<h1><?php
    if (isset($_GET['categorie'])) {
        switch ($_GET['categorie']) {
            case 'all':
                echo 'Accueil';
                break;
            case '1':
                echo 'Animaux';
                break;
            case '2':
                echo 'Repas';
                break;
            case '3':
                echo 'Monuments';
                break;
        }
    } else {
        echo 'Accueil';
    }

    ?></h1>


<!--  Affichage des photos -->
<?php
foreach ($photos as $photo) {
    echo '<div class="col-md-4">';
    echo '<div class="thumbnail">';
    echo '<a href="index.php?page=photo&id=' . $photo['photoId'] . '">';
    echo '<img src="' . PATH_IMAGES . $photo['nomFich'] . '" alt="' . $photo['nomFich'] . '" style="width:100%">';
    echo '</a>';
    echo '</div>';
    echo '</div>';
}
?>

<!--  Fin de la page -->

<!--  Pied de page -->
<?php require_once(PATH_VIEWS . 'footer.php');
