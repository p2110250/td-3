<?php require_once(PATH_VIEWS . 'header.php'); ?>

<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS . 'alert.php'); ?>

<h2>Quelle photo ?</h2>

<form action="index.php?page=ajout" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="photo">Photo</label>
        <input type="file" class="form-control" id="photo" name="photo" placeholder="Photo">
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control" id="description" name="description" placeholder="Description">
    </div>
    <div class="form-group">
        <label for="categorie">Catégorie</label>
        <select name="categorie" id="categorie">
            <option value="1">Animaux</option>
            <option value="2">Repas</option>
            <option value="3">Monuments</option>
        </select>
    </div>
    <button type="submit" class="btn btn-default">Envoyer</button>
</form>


<!--  Pied de page -->
<?php require_once(PATH_VIEWS . 'footer.php');
