<?php require_once(PATH_VIEWS . 'header.php'); ?>

<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS . 'alert.php'); ?>

<h2>Connexion pour modifer le catalogue</h2>

<?php if (!isset($_SESSION['logged'])) { ?>
    <form method="post" action="index.php?page=connexion">
        <div class="form-group">
            <label for="login">Login</label>
            <input type="text" class="form-control" id="login" name="login" placeholder="Login">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
<?php } else { ?>
    <form method="post" action="index.php?page=connexion">
        <button type="submit" class="btn btn-default">Deconnexion</button>
        <input type="hidden" id="deconnexion" name="deconnexion" value="deconnexion">
    </form>
<?php } ?>



<!--  Pied de page -->
<?php require_once(PATH_VIEWS . 'footer.php');
