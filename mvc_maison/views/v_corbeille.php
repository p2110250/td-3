<?php require_once(PATH_VIEWS . 'header.php'); ?>

<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS . 'alert.php'); ?>

<!--  Début de la page -->
<h1>Corbeille</h1>

<!--  Tout supprimer ou tout restaurer   -->
<?php if (isset($photos) && !empty($photos)) { ?>
    <div>
        <a href="index.php?page=corbeille&deleteAll" class="btn btn-danger">Vider la corbeille</a>
        <a href="index.php?page=corbeille&restoreAll" class="btn btn-success">Tout restaurer</a>
    </div>
<?php } ?>


<!--  Affichage des photos dans la corbeille -->
<?php
foreach ($photos as $photo) {
    echo '<div class="col-md-4">';
    echo '<div class="thumbnail">';
    echo '<img src="' . PATH_IMAGES . $photo['nomFich'] . '" alt="' . $photo['description'] . '">';
    echo '<div class="caption">';
    echo '<h3>' . $photo['description'] . '</h3>';
    echo '<p>Catégorie : <a href="index.php?page=accueil&categorie=' . $photo['catId'] . '">' . $photo['nomCat'] . '</a></p>';
    echo '<p>Nom du fichier : ' . $photo['nomFich'] . '</p>';
    echo '<p><a href="index.php?page=corbeille&restore=' . $photo['photoId'] . '" class="btn btn-primary" role="button">Restaurer</a> <a href="index.php?page=corbeille&delete=' . $photo['photoId'] . '" class="btn btn-danger" role="button">Supprimer</a></p>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
}
?>


<!--  Pied de page -->
<?php require_once(PATH_VIEWS . 'footer.php'); ?>