<!DOCTYPE html>
<html>

<head>
	<title><?= TITRE ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="Language" content="<?= LANG ?>" />
	<meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1; user-scalable=0" />

	<link href="<?= PATH_CSS ?>bootstrap.css" rel="stylesheet">
	<link href="<?= PATH_CSS ?>css.css" rel="stylesheet">

	<script type="text/javascript" src="<?= PATH_SCRIPTS ?>jquery-3.1.1.js"></script>
	<script type="text/javascript" src="<?= PATH_SCRIPTS ?>jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?= PATH_SCRIPTS ?>monjs.js"></script>
</head>

<body>
	<?php
	session_start();
	if (isset($_POST['login']) && isset($_POST['password']) && !isset($_POST['deconnexion'])) {
		if ($_POST['login'] == 'admin' && $_POST['password'] == 'p4ss4dm1n') {
			$_SESSION['logged'] = true;
			$alert['messageAlert'] = 'Vous êtes connecté';
			$alert['classAlert'] = 'success';
		} elseif ($_POST['login'] == 'admin' && $_POST['password'] != 'p4ss4dm1n') {
			$alert['messageAlert'] = "Mot de passe incorrect";
		} elseif ($_POST['login'] != 'admin') {
			$alert['messageAlert'] = "Identifiant incorrect";
		}
	} else {
		if (isset($_POST['deconnexion'])) {
			session_destroy();
			$alert['messageAlert'] = "Vous êtes déconnecté";
			$alert['classAlert'] = 'success';
		}
	}

	?>
	<!-- En-tête -->
	<header class="header">
		<section class="container">
			<div class="row">
				<div class="col-md-2 col-sm-2 col-xs-12">
					<img src="<?= PATH_LOGO ?>" alt="<?= LOGO ?>" class="img-circle">
				</div>
				<div class="col-md-10 col-sm-10 col-xs-12">
					<h2><?= TITRE ?></h2>
				</div>
			</div>
		</section>
	</header>
	<!-- Menu -->
	<?php include(PATH_VIEWS . 'menu.php'); ?>
	<!-- Vue -->
	<section class="container">
		<div class="row">