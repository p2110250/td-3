<?php
/*
 * Copyright 2016, Eric Dufour
 * http://techfacile.fr
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 * menu: http://www.w3schools.com/bootstrap/bootstrap_ref_comp_navs.asp
 */
?>
<!-- Menu du site -->

<nav class="navbar navbar-default">
	<div class="container-fluid">
		<ul class="nav navbar-nav">
			<li <?php echo ($page == 'accueil' ? 'class="active"' : '') ?>>
				<a href="index.php">
					<?= MENU_ACCUEIL ?>
				</a>
			</li>
			<li <?php echo ($page == 'corbeille' ? 'class="active"' : '') ?>>
				<a href="index.php?page=corbeille">
					<?= 'Corbeille' ?>
				</a>

			</li>
			<?php if (isset($_SESSION['logged'])) { ?>
				<li>
					<a href="index.php?page=ajout">
						<?= 'Ajouter une photo' ?>
					</a>
				</li>
			<?php } ?>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<?php if (!isset($_SESSION['logged'])) { ?>
				<li <?php echo ($page == 'connexion' ? 'class="active"' : '') ?>>
					<a href="index.php?page=connexion">
						<?php echo  'Connexion';  ?>
					</a>
				</li>
			<?php } else { ?>
				<li <?php echo ($page == 'connexion' ? 'class="active"' : '') ?>>
					<a href="index.php?page=connexion">
						<?php echo  'Deconnexion'; ?>
					</a>
				</li>
			<?php } ?>

		</ul>
	</div>
</nav>