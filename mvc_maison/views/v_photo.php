<?php require_once(PATH_VIEWS . 'header.php'); ?>

<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS . 'alert.php'); ?>

<!--  Début de la page -->
<h1><?= $photo[0]['nomCat'] ?></h1>

<!--  Affichage de la photo -->
<?php
echo '<div class="col-md-4">';
echo '<div class="thumbnail">';
echo '<img src="' . PATH_IMAGES . $photo[0]['nomFich'] . '" alt="' . $photo[0]['description'] . '">';
echo '</div>';
echo '</div>';
?>

<!--  Affichage de sa description, catégorie et nom du fichier -->
<div class="col-md-8">
    <div class="thumbnail">
        <div class="caption">
            <h3><?= $photo[0]['description'] ?></h3>
            <p>Catégorie : <a href="index.php?page=accueil&categorie=<?= $photo[0]['catId'] ?>"><?= $photo[0]['nomCat'] ?></a></p>
            <p>Nom du fichier : <?= $photo[0]['nomFich'] ?></p>
        </div>
    </div>
</div>

<!--  Mettre à la corbeille -->
<p><a href="index.php?page=corbeille&incorb=<?php echo $_GET['id'] ?>" class="btn btn-danger" role="button">Mettre à la corbeille</a></p>


<!--  Fin de la page -->

<!--  Pied de page -->
<?php require_once(PATH_VIEWS . 'footer.php');
